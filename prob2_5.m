% Problem 2.5
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Neighborhood Code
getData(3,[1,1])
nhCode = getData(3,[2,N]);
% Location value
getData(43,[1,1])   
loc = getData(43,[2,N]);
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

invalidloc = cellfun(@isempty, loc);
invalidNhCode = cellfun(@isempty, nhCode);
invalidYear = isnan(year);
loc(invalidloc | invalidNhCode | invalidYear) = [];
nhCode(invalidloc | invalidNhCode | invalidYear) = [];
blkNum(invalidloc | invalidNhCode | invalidYear) = [];
year(invalidloc | invalidNhCode | invalidYear) = [];
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
[uYear,firstIdxYear,keyYear] = unique(year);
[uNhCode,firstIdxNhCode,keyNhCode] = unique(nhCode);
lat = zeros(numel(loc),1);
long = zeros(numel(loc),1);
for k = 1:numel(loc)
    C = textscan(loc{k},'"(%f, %f)"');
    lat(k) = C{1};
    long(k) = C{2};
    %fprintf(1,'lat = %.13f, long = %.13f\n',lat(k),long(k))
end
pruneLat = zeros(numel(uBlkNum),1);
pruneLong = zeros(numel(uBlkNum),1);
pruneKeyNhCode = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    [~,idx] = max(year(k==keyBlkNum));
    
    tmp = lat(keyBlkNum==k);
    pruneLat(k) = tmp(idx);
    tmp = long(keyBlkNum==k);
    pruneLong(k) = tmp(idx);
    tmp = keyNhCode(keyBlkNum==k);
    pruneKeyNhCode(k) = tmp(idx);
end
stdLat = zeros(numel(uNhCode),1);
stdLong = zeros(numel(uNhCode),1);
area = zeros(numel(uNhCode),1);
for k = 1:numel(uNhCode)
    stdLat(k) = std(pruneLat(k==pruneKeyNhCode));
    stdLong(k) = std(pruneLong(k==pruneKeyNhCode));
    dkm1 = lldistkm([0 0],[stdLat(k) 0]);
    dkm2 = lldistkm([0 0],[0 stdLong(k)]);
    area(k) = dkm1*dkm2;
end
if isPlot
    figure; plot(area);
end
fprintf(1,'The area, in square kilometers, of the largest neighborhood = %.10f\n',max(area));