% Problem 3
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; %close all;

rootDir = 'C:/cygwin64/home/UyenBui/';
addpath([rootDir 'jsonlab/'])
addpath([rootDir 'V1_1_urlread2/']);
addpath([rootDir 'sas-clientLib/src/']);
isPlot = true;

%{
% Overview
for k = 1:52;
    str = getCSVData('2013_NYC_Pothole_Complaints_-_Heat_Map.csv',k,[1,1]);
    fprintf(1,'%d: %s\n',k, str{1});
end

% SF Parking
params = {'lat','37.792275','long','-122.397089','radius','0.25','uom','mile','response','json'};
queryString =  http_paramsToString(params);
tmp = urlread2(['http://api.sfpark.org/sfpark/rest/availabilityservice?' queryString]);

% Freesound
params = {'filter','{!geofilt sfield=geotag pt=37.7749300,-122.4194200 d=100} created:[* TO NOW]',...
    'fields','geotag,tags,created'};
queryString =  http_paramsToString(params);
tmp = urlread2(['http://www.freesound.org/apiv2/search/text/?' queryString]);
resp = loadjson(tmp);
%}

data = loadjson('san_francisco.json'); stateName = {'California'};
%data = loadjson('chicago.json'); stateName = {'Illinois','Wisconsin','Michigan','Indiana','Ohio','Iowa','Missouri'};

tags = cell(numel(data.results),1);
time = zeros(numel(data.results),1);
loc = zeros(numel(data.results),2);
for k = 1:numel(data.results)
    tags{k} = data.results{k}.tags;
    time(k) = datenum8601(data.results{k}.created);
    loc(k,:) = sscanf(data.results{k}.geotag,'%f')';
end

% stopwords
stopwords={'field-recording','Field-recording','aguasonic','aip09','california','California',...
    'san-francisco','San-Francisco','ORTF','usa','san-francisco-bay','kerouac',...
    'potsdam','stanford','stanford-university','sherman-island','suny','Point-Reyes','schollenberger',...
    'america','Illinois','Santa','chicago','Chicago','h4','at825'};
for k = 1:numel(tags)
    tags{k}(ismember(tags{k},stopwords)) = [];
end
% meaningful words
for k = 1:numel(tags)
    tags{k}(ismember(tags{k},'ambience')) = {'ambiance'};
    tags{k}(ismember(tags{k},'ambient')) = {'ambiance'};
end
% all tags analysis
flatTags = [tags{:}];
[uFlatTags,firstIdxFlatTags,keyFlatTags] = unique(flatTags);
n = accumarray(keyFlatTags,1);
if isPlot
    figure; plot(n)
    for k = 1:numel(uFlatTags)
        text(k,n(k),uFlatTags(k))
    end
end

%% geo-based analysis
states = geoshape(shaperead('usastatehi', 'UseGeoCoords', true));
ind = false(1,51);
for k = 1:numel(stateName)
    ind = ind | strcmp(states.Name, stateName{k});
end
ma = states(ind);
K = 5; % number of clusters
rng(1);
[idxC,C] = kmedoids(loc,K);
if isPlot
    figure; hold on;
    geoshow(ma, 'LineWidth', 1.5, 'FaceColor', [.5 .8 .6])
    colorCode = 'rgbmcky';
    for k = 1:K
        plot(loc(idxC == k,2),loc(idxC == k,1),[colorCode(k) 'o'])
        text(C(k,2),C(k,1),sprintf('Cluster %d',k))
    end
end

topCTags = cell(K,1);
for k = 1:K
    cTags = tags(idxC==k);
    flatCTags = [cTags{:}];
    keyFlatCTags = zeros(numel(flatCTags),1);
    for l = 1:numel(flatCTags)
        keyFlatCTags(l) = find(strcmp(flatCTags{l},uFlatTags));
    end
    n = accumarray(keyFlatCTags,1);
    [~,idxSortN] = sort(n,'descend');
    topCTags{k} = uFlatTags(idxSortN(1:8));
    if isPlot
        figure; plot(n)
        for l = 1:numel(n)
            if n(l) > 0 % ignore zero-count labels
                text(l,n(l),uFlatTags(l))
            end
        end
        title(sprintf('Cluster %d',k))
    end
end

if isPlot
    figure; hold on;
    geoshow(ma, 'LineWidth', 1.5, 'FaceColor', [.5 .8 .6])
    colorCode = 'rgbmcky';
    for k = 1:K
        plot(loc(idxC == k,2),loc(idxC == k,1),[colorCode(k) 'o'])
        th = text(C(k,2),C(k,1),[sprintf('Cluster %d:',k) sprintf('\n%s',topCTags{k}{:})]);
        set(th,'fontsize',10,'fontweight','bold');
    end
    title(sprintf('Dataset of %d audio clips',numel(tags)))
    xlabel('Longitude'); ylabel('Latitude');
    set(gca,'fontsize',16);
end

%% temporal-based analysis
nTime = time-min(time);
if isPlot
    figure; hist(nTime)
    ylabel('Serial date number')
    title(sprintf('Starting time is %s',datestr(min(time))));
end

Kt = 5;
[idxCt,Ct] = kmedoids(nTime,Kt);
% sort in increasing time
[Ct,sIdx] = sort(Ct);
tmp = zeros(size(idxCt));
for k = 1:Kt
    tmp(idxCt == k) = sIdx(k);
end
idxCt = tmp;

topCtTags = cell(K,1);
for k = 1:K
    ctTags = tags(idxCt==k);
    flatCtTags = [ctTags{:}];
    keyFlatCtTags = zeros(numel(flatCtTags),1);
    for l = 1:numel(flatCtTags)
        keyFlatCtTags(l) = find(strcmp(flatCtTags{l},uFlatTags));
    end
    n = accumarray(keyFlatCtTags,1);
    [~,idxSortN] = sort(n,'descend');
    topCtTags{k} = uFlatTags(idxSortN(1:8));
    if isPlot
        figure; plot(n)
        for l = 1:numel(n)
            if n(l) > 0 % ignore zero-count labels
                text(l,n(l),uFlatTags(l))
            end
        end
        title(sprintf('Cluster %d',k))
    end
end

if isPlot
    figure;
    plot(Ct,zeros(size(Ct)),'o-')
    for k = 1:Kt
        th = text(Ct(k),-0.25,[sprintf('%s',datestr(Ct(k)+min(time))) sprintf('\n%s',topCtTags{k}{:})]);
        set(th,'fontsize',10,'fontweight','bold');
    end
    xlabel('Serial date number')
    set(gca,'YTickLabel','')
end