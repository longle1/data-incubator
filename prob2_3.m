% Problem 2.3
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Neighborhood Code
getData(3,[1,1])
nhCode = getData(3,[2,N]);
% Improvement value
getData(37,[1,1])   
imVal = str2double(getData(37,[2,N]));
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

invalidNhCode = cellfun(@isempty, nhCode);
zeroImVal = imVal == 0;
invalidYear = isnan(year);
imVal(invalidNhCode | zeroImVal | invalidYear) = [];
nhCode(invalidNhCode | zeroImVal | invalidYear) = [];
year(invalidNhCode | zeroImVal | invalidYear) = [];
blkNum(invalidNhCode | zeroImVal | invalidYear) = [];
[uNhCode,firstIdxNhCode,keyNhCode] = unique(nhCode);
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
[uYear,firstIdxYear,keyYear] = unique(year);
pruneImVal = zeros(numel(uBlkNum),1);
pruneKeyNhCode = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    [~,idx] = max(year(k==keyBlkNum));
    
    tmp = imVal(k==keyBlkNum);
    pruneImVal(k) = tmp(idx);
    tmp = keyNhCode(k==keyBlkNum);
    pruneKeyNhCode(k) = tmp(idx);
end
accImVal = accumarray(pruneKeyNhCode,pruneImVal);
nImVal = accumarray(pruneKeyNhCode,1);
avgImVal = accImVal./nImVal;
if isPlot
    figure; plot(avgImVal)
end
fprintf(1,'The difference between the greatest and least average values is %.10f\n',max(avgImVal)-min(avgImVal));