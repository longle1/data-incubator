% Problem 2.2
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Improvement value
getData(37,[1,1])   
imVal = str2double(getData(37,[2,N]));
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

zeroImVal = imVal == 0;
invalidYear = isnan(year);
imVal(zeroImVal | invalidYear) = [];
year(zeroImVal | invalidYear) = [];
blkNum(zeroImVal | invalidYear) = [];
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
[uYear,firstIdxYear,keyYear] = unique(year);
pruneImVal = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    [~,idx] = max(year(k==keyBlkNum));
    
    tmp = imVal(k==keyBlkNum);
    pruneImVal(k) = tmp(idx);
end
if isPlot
    figure; plot(pruneImVal)
end
fprintf(1,'The median assessed improvement value is %.10f\n',median(pruneImVal));