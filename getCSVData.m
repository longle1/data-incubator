function data = getCSVData(filename,colIdx, lineRange)
% Read data from a CSV dataset
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
fid = fopen(filename);

cnt = 1;
while cnt < lineRange(1)
    fgetl(fid);
    cnt = cnt + 1;
end

data = cell(numel(lineRange(2)-lineRange(1)+1),1);
while cnt <= lineRange(2)
    line = fgetl(fid);
    cnt = cnt + 1;
    % [1] https://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes/1757107#1757107
    tokens = strsplit(line,',(?=([^\"]*\"[^\"]*\")*[^\"]*$)','CollapseDelimiters',0,'DelimiterType', 'RegularExpression');
    data{cnt-lineRange(1)} = tokens{colIdx};
end

fclose(fid);