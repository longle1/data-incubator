function data = getData(colIdx, lineRange)
% Read data from the dataset
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

switch colIdx
    case 7
        filename = 'classCode';
    case 37
        filename = 'imVal';
    case 5
        filename = 'blkNum';
    case 1
        filename = 'year';
    case 3
        filename = 'nhCode';
    case 38
        filename = 'landVal';
    case 43
        filename = 'loc';
    case 14
        filename = 'numUnit';
    case 9
        filename = 'yearBuilt';
    case 11
        filename = 'numBed';
    case 40
        filename = 'zipCode';
    case 22
        filename = 'lotArea';
    case 20
        filename = 'propArea';
    otherwise
        filename = '';
end

%{
if exist(['logs/' filename '.mat'], 'file') == 2
    tmp = load(['logs/' filename '.mat'], filename);
    data = tmp.(filename);
else    
%}
    fid = fopen('Historic_Secured_Property_Tax_Rolls.csv');

    cnt = 1;
    while cnt < lineRange(1)
        fgetl(fid);
        cnt = cnt + 1;
    end

    data = cell(numel(lineRange(2)-lineRange(1)+1),1);
    while cnt <= lineRange(2)
        line = fgetl(fid);
        cnt = cnt + 1;
        % [1] https://stackoverflow.com/questions/1757065/java-splitting-a-comma-separated-string-but-ignoring-commas-in-quotes/1757107#1757107
        tokens = strsplit(line,',(?=([^\"]*\"[^\"]*\")*[^\"]*$)','CollapseDelimiters',0,'DelimiterType', 'RegularExpression');
        data{cnt-lineRange(1)} = tokens{colIdx};
    end

    fclose(fid);
    
    if ~isempty(filename) && lineRange(2)-lineRange(1)+1 > 1e3
        eval(sprintf('%s = data;',filename));
        save(['logs/' filename '.mat'], filename);
    end
%end