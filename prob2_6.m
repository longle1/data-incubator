% Problem 2.6
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Number of Units
getData(14,[1,1])
numUnit = str2double(getData(14,[2,N]));
% Year Property Built
getData(9,[1,1])
yearBuilt = str2double(getData(9,[2,N]));
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

invalidYearBuilt = isnan(yearBuilt);
zeroUnit = numUnit == 0;
invalidYear = isnan(year);
numUnit(invalidYearBuilt | zeroUnit | invalidYear) = [];
yearBuilt(invalidYearBuilt | zeroUnit | invalidYear) = [];
blkNum(invalidYearBuilt | zeroUnit | invalidYear) = [];
year(invalidYearBuilt | zeroUnit | invalidYear) = [];
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
earliestNumUnit = zeros(numel(uBlkNum),1);
earliestYearBuilt = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    [~,idx] = min(year(k==keyBlkNum));
    
    tmp = numUnit(k==keyBlkNum);
    earliestNumUnit(k) = tmp(idx);
    tmp = yearBuilt(k==keyBlkNum);
    earliestYearBuilt(k) = tmp(idx);
end
fprintf(1,'The difference between the average number of units in buildings build in or after 1950, and that for buildings built before 1950 = %.10f\n', ...
    mean(earliestNumUnit(earliestYearBuilt>=1950)) - mean(earliestNumUnit(earliestYearBuilt<1950)) );