% Problem 2.8
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Zipcode of Parcel
getData(40,[1,1])   
zipCode = getData(40,[2,N]);
% Lot Area
getData(22,[1,1])   
lotArea = str2double(getData(22,[2,N]));
% Property Area in Square Feet
getData(20,[1,1])   
propArea = str2double(getData(20,[2,N]));
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);

invalidZipCode = cellfun(@isempty, zipCode);
zeroLotArea = lotArea == 0;
zeroPropArea = propArea == 0;
lotArea(invalidZipCode | zeroLotArea | zeroPropArea) = [];
propArea(invalidZipCode | zeroLotArea | zeroPropArea) = [];
zipCode(invalidZipCode | zeroLotArea | zeroPropArea) = [];
blkNum(invalidZipCode | zeroLotArea | zeroPropArea) = [];
[uZipCode,firstIdxZipCode,keyZipCode] = unique(zipCode);
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
pruneLotArea = zeros(numel(uBlkNum),1);
prunePropArea = zeros(numel(uBlkNum),1);
pruneKeyZipCode = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    % multiple lotArea and propArea should be the same
    
    tmp = lotArea(keyBlkNum==k);
    pruneLotArea(k) = tmp(1);
    tmp = propArea(keyBlkNum==k);
    prunePropArea(k) = tmp(1);
    tmp = keyZipCode(keyBlkNum==k);
    pruneKeyZipCode(k) = tmp(1);
end
accLotArea = accumarray(pruneKeyZipCode,pruneLotArea);
accPropArea = accumarray(pruneKeyZipCode,prunePropArea);
if isPlot
    figure; plot(accPropArea./accLotArea);
end
fprintf(1,'The largest ratio of property area to lot area  = %.10f\n', max(accPropArea./accLotArea));