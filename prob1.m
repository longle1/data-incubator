% Problem 1
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;

%T = 10;
T = 2^10;
a = [4 6; 
     6 8;
     7 9;
     4 8;
     3 9;
     0 0;
     1 7;
     2 6;
     1 3;
     4 2]+1; % index starting from 1
 
rng(1);
N = 1e9;
S = zeros(N,1);
parfor l = 1:N
    s = ones(1,T); % state
    for k = 2:T
        if rand() < 0.5
            s(k) = a(s(k-1),1);
        else
            s(k) = a(s(k-1),2);
        end
    end
    S(l) = sum(s-1);
    %{
    figure;
    subplot(211); plot(s-1,'o-'); ylabel('state')
    subplot(212); plot(cumsum(s-1)); ylabel('Sum')
    xlabel('steps')
    %}
end
fprintf(1,'T = %d\n',T);
fprintf(1,'mean = %.10f\n',mean(mod(S,T)));
fprintf(1,'std = %.10f\n',std(mod(S,T)));

if T == 10
    i5 = mod(S,5)==0; % indicator div 5
    i7 = mod(S,7)==0; % indicator div 7
    P5_7 = sum(i5 & i7)/sum(i7);
    fprintf(1,'P(div5|div7) = %.10f\n',P5_7);
elseif T == 2^10
    i23 = mod(S,23)==0; % indicator div 23
    i27 = mod(S,27)==0; % indicator div 27
    P23_27 = sum(i23 & i27)/sum(i27);
    fprintf(1,'P(div23|div27) = %.10f\n',P23_27);
end
