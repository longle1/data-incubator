% Problem 2.7
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Number of Units
getData(14,[1,1])
numUnit = str2double(getData(14,[2,N]));
% Number of Bedrooms
getData(11,[1,1])
numBed = str2double(getData(11,[2,N]));
% Zipcode of Parcel
getData(40,[1,1])   
zipCode = getData(40,[2,N]);
% Block and Lot Number
getData(5,[1,1])
blkNum = getData(5,[2,N]);
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

invalidZipCode = cellfun(@isempty, zipCode);
zeroBed = numBed == 0;
zeroUnit = numUnit == 0;
invalidYear = isnan(year);
zipCode(invalidZipCode | zeroBed | zeroUnit | invalidYear) = [];
numBed(invalidZipCode | zeroBed | zeroUnit | invalidYear) = [];
numUnit(invalidZipCode | zeroBed | zeroUnit | invalidYear) = [];
blkNum(invalidZipCode | zeroBed | zeroUnit | invalidYear) = [];
year(invalidZipCode | zeroBed | zeroUnit | invalidYear) = [];
[uZipCode,firstIdxZipCode,keyZipCode] = unique(zipCode);
[uBlkNum,firstIdxBlkNum,keyBlkNum] = unique(blkNum);
[uYear,firstIdxYear,keyYear] = unique(year);
pruneNumBed = zeros(numel(uBlkNum),1);
pruneNumUnit = zeros(numel(uBlkNum),1);
pruneKeyZipCode = zeros(numel(uBlkNum),1);
for k = 1:numel(uBlkNum)
    [~,idx] = max(year(k==keyBlkNum));
    
    tmp = numBed(keyBlkNum==k);
    pruneNumBed(k) = tmp(idx);
    tmp = numUnit(keyBlkNum==k);
    pruneNumUnit(k) = tmp(idx);
    tmp = keyZipCode(keyBlkNum==k);
    pruneKeyZipCode(k) = tmp(idx);
end
accNumBed = accumarray(pruneKeyZipCode,pruneNumBed);
accNumUnit = accumarray(pruneKeyZipCode,pruneNumUnit);
nNum = accumarray(pruneKeyZipCode,1);
avgNumBed = accNumBed./nNum;
avgNumUnit = accNumUnit./nNum;
if isPlot
    figure; plot(avgNumBed./avgNumUnit);
end
fprintf(1,'The maximal average number of bedrooms per unit in a zip code = %.10f\n',max(avgNumBed./avgNumUnit));