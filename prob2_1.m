% Problem 2.1
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Overview
for k = 1:43;
    str = getData(k,[1,1]);
    fprintf(1,'%d: %s\n',k, str{1});
end

% Property Class Code
getData(7,[1,1])
classCode = getData(7,[2,N]);

invalidClassCode = cellfun(@isempty, classCode);
classCode(invalidClassCode) = [];
% hash input to keyIn, also has first indices as a by-product
[uClassCode,firstIdxClassCode,keyClassCode] = unique(classCode);
n = accumarray(keyClassCode,1);
if isPlot
    figure; plot(n)
end
[~,idx] = max(n);
%fprintf(1,'The most common class of properties is %s\n',uClassCode{idx});
fprintf(1,'Fraction of assessments for properties of the most common class is %.10f\n',n(idx)/sum(n));