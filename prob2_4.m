% Problem 2.4
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
warning('off', 'MATLAB:REGEXP:ErrorEscapedLiteral')
N = 1612111;
isPlot = false;

% Land Values 
getData(38,[1,1])
landVal = str2double(getData(38,[2,N]));
% Closed Roll Fiscal Year
getData(1,[1,1])
year = str2double(getData(1,[2,N]));

zeroLandValIdx = landVal == 0;
invalidYear = isnan(year);
year(zeroLandValIdx | invalidYear) = [];
landVal(zeroLandValIdx | invalidYear) = [];
[uYear,firstIdxYear,keyYear] = unique(year);
B = zeros(numel(landVal),1);
A = ones(numel(landVal),2);
for k =1:numel(uYear)
    B(keyYear == k) = log(landVal(keyYear == k));
    A(keyYear == k,2) = k-1;
end
params = A\B;
logLandVal0 = params(1);
r = params(2);
if isPlot
    figure; hold on;
    for k =1:numel(uYear)
        plot((k-1)*ones(size(B(keyYear == k))),B(keyYear == k), 'bx');
    end
    plot((0:numel(uYear)-1)',logLandVal0+r*(0:numel(uYear)-1)','r--')
end
fprintf(1,'The yearly growth rate of Land Values over all assessments are P_0 = %.10f, r = %.10f\n',exp(logLandVal0),r);